const int LEVELS = 5;
const int LEVEL_PINS = {4, 6, 8, 10, 12};
const int REFRESH_WAIT = 16;

void setup(){
  Serial.begin(9600);
  for(int i = 0; i<LEVELS; i++)
    pinMode(LEVEL_PINS[i], INPUT_PULLUP);
}

int getLevel(){
  for(int i = 0; i<5; i++){
    if(digitalRead(LEVEL_PINS[i]) == LOW)
      return i;
  }
  return -1;
}

void loop(){
  Serial.println(getLevel());
  delay(REFRESH_WAIT);
}
