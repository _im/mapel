import fs from 'fs';
import WebSocket from 'ws';
import { SerialPort } from 'serialport'
import { ReadlineParser } from '@serialport/parser-readline'

// let config = JSON.parse(fs.readFileSync(process.argv[2] || "../config.json", {encoding : "utf-8"}))
let PORT = process.argv[2] || 9080

let socket = null
let serial = null

function serial_line_to_message(line){
  let level_index = parseInt(line)
  if(isNaN(level_index)){
    console.log("bad level format '" + line + "'")
    return null
  }else{
    return JSON.stringify( [ 0,level_index ] )
  }
}

function port_matching(port){
  if(port.manufacturer && port.manufacturer.includes("Arduino")){
    return true
  }else{
    return false
  }
}

function on_connect(){
  console.log("connected to ws://localhost:" + PORT)
}

function try_reconnect(){

}

function on_disconnect(){
  console.log("disconnected")
  socket = null
}

function on_message(m){
  // console.log(m)
}

function open_serial_port(port_path, callback){
  if(!port_path)
    return null
  else{
    let p = new SerialPort({path : port_path, baudRate : 9600})
      .pipe(new ReadlineParser({ delimiter: '\r\n' }))
    return p
  }
}

async function find_port(matching){
  let ports = await SerialPort.list()
  // console.log(ports)
  for(let p of ports){
    if(matching(p)){
      console.log(p.path)
      return p.path
    }
  }
  return null
}


function try_send_message(line){

  if(socket && socket.readyState == 1){

    let m = serial_line_to_message(line)
    if(m) {
      // console.log(line)
      socket.send(m)
      return true
    }else{
      return false
    }
  }
}

async function try_connections(){
  if(socket == null){
    try{
      socket = new WebSocket("ws://localhost:" + PORT)
        .on("open", on_connect)
        .on("close", on_disconnect)
        // .on("message", console.log)
      socket.onerror = e => console.log("no server found at :", PORT)
    }catch(e){
      // console.log(e)
    }
  }

  if(serial == null){
    let port = await find_port( port_matching )
    console.log("find ", port)
    serial = open_serial_port(port)
    if(serial){
      serial.on("data", try_send_message)
      console.log("found matching serial device ", )
    }else{
      console.log("no serial device was found, retrying...")
    }
  }

  setTimeout(try_connections, 2000)
}

async function init(){
  try_connections()
  // KEYBOARD
  // let stdin = process.stdin;
  // stdin.setRawMode( true );
  // stdin.resume();
  // stdin.setEncoding( 'utf8' );
  // stdin.on( 'data', function( key ){
  //   if ( key === '\u0003' ) {
  //     process.exit();
  //   }else{
  //     try_send_message(key)
  //   // process.stdout.write( key );
  //   }
  // });
}

// async function list() {
//   let ports = await SerialPort.list()
//   console.log(ports)
// }

// list()


init()