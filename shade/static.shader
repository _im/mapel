shader_type canvas_item;

uniform vec4 default_color : hint_color = vec4(1.0);
uniform float pulse = 0.0;
uniform float freq = 1.0;

varying float wave;

void vertex(){
  wave = 0.5 + 0.5 * sin(TIME * freq * 2.0 * 3.14159265);
}

void fragment(){
  COLOR = default_color;
  COLOR.a = mix(1.0, wave, pulse);
}