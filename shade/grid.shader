shader_type canvas_item;

uniform vec4 solved_color : hint_color = vec4(1,0,0,1);
uniform vec4 color_a : hint_color = vec4(0,1,0,1);
uniform vec4 color_b : hint_color = vec4(0,0,1,1);
uniform vec4 back_a : hint_color = vec4(0.7,0.0,1.0,1.0);
uniform vec4 back_b : hint_color = vec4(0.0,0.0,1.0,1);
uniform vec2 cursor;

uniform float hovered = -1.0;

uniform float offset;
uniform float ratio = 1.5;
uniform float stroke_width = 0.1;
uniform float freq = 100.0;
uniform float speed = 1.0;
uniform float dir = -1.0;


void fragment(){
  vec2 uv = vec2(SCREEN_UV.x, 1.0 - SCREEN_UV.y);
  float d = distance(uv, cursor);

  vec3 ab = mix(color_a.rgb, color_b.rgb, d * 0.25);
  vec3 ba = mix(color_b.rgb, color_a.rgb, d * 0.25);

  float x = (uv.x + 1.0) * 0.5;
  x = x * freq + TIME * speed * dir;
  x = x - floor(x);
  x = step(x, stroke_width);

  float y = (uv.y + 1.0) * 0.5;
  y = y * freq * ratio;
  y = y - floor(y);
  y = step(y, stroke_width * 8.0);

  COLOR = vec4(mix(ab, ba, d) * (x - y), 1.0);
}