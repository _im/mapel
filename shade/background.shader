shader_type canvas_item;

uniform sampler2D solved;
uniform vec4 solved_color : hint_color = vec4(1,0,0,1);
uniform vec4 color_a : hint_color = vec4(0,1,0,1);
uniform vec4 color_b : hint_color = vec4(0,0,1,1);
uniform vec4 back_a : hint_color = vec4(0.7,0.0,1.0,1.0);
uniform vec4 back_b : hint_color = vec4(0.0,0.0,1.0,1);
uniform vec2 cursor;

uniform float hovered = -1.0;
uniform float grabbed = 0.0;

uniform float offset;

uniform float win_time = 0.0;
uniform float win_rate = 10.0;

void fragment(){
  vec4 ca = mix(back_a, color_a, COLOR.g);
  vec4 cb = mix(back_b, color_b, COLOR.g);
  float time = TIME * 0.1;
  float d = distance(UV, cursor);
  float time2 = fract(time + MODULATE.r);
  float t = time2 > 0.5 ? 0.5 - (time2 - 0.5) : time2;
  
  vec3 ab = mix(ca, cb, d * 0.5).rgb;
  vec3 ba = mix(cb, ca, d * 0.5).rgb;
  float hover = 1.0 - step(0.01, abs(hovered - COLOR.b));
  vec3 c = mix(ab, vec3(1.0), t * 0.7 + hover * 0.3);
  
  vec4 modulate = COLOR;
  float h = hover * (0.5 + grabbed * 0.5);
  COLOR = vec4(
    mix(
      mix(ab, ba, t), 
      c, 
      h * 0.5 + 0.5 * h * (0.5 + 0.5 * sin(TIME * 3.14159265 * 2.0))),
    1.0);

  vec4 solved_c = texture(solved, vec2(fract(modulate.b + win_time * win_rate), 0.0));
  COLOR = mix(COLOR, solved_c, modulate.r);
}
