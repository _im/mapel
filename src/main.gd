class_name Main
extends Node2D

enum Cmd {
	select_level
	move,
	place,
	remove,
	grab,
	drop,
	turn,
	finish,
	set_editing,
	explode,
	undo,
	init,
	shake,
	start,
	set_static,
	show_rotate
}

var config : Mapel.Config
var state : Mapel.State
var server : MapelServer
var commands = []
var command_stack = []

var input_stack = ""
var edit_cheat = "1122115555"

var node_pid = null

onready var level = $Level
onready var editor = $Editor
onready var Tutorial = $Tutorial
onready var background = $BackgroundContainer/Background
onready var cursor_timer = $CursorTimer

export var level_sound : AudioStreamOGGVorbis

var grid_material = preload("res://mats/grid.tres")

var level_materials = [
	preload("res://mats/level_0.tres"),
	preload("res://mats/level_1.tres"),
	preload("res://mats/level_2.tres"),
	preload("res://mats/level_3.tres"),
	preload("res://mats/level_4.tres"),
]

static func save(s : Mapel.State):
	Util.save_json("user://world.json", s.save())


func print_ip():
	var result = []
	OS.execute("hostname", ["-i"], true, result)
	$IP.text = result[0] + "\n" + "~/.local/share/godot/app_userdata/mapel"
	$IP.visible = true
	if state.editing:
		run(Cmd.set_static, not state.static)

func update_tutorial(s : Mapel.State):
	if s.editing:
		Tutorial.update_labels(
			"move", 
			"place", 
			"shapes" if s.static else "static", 
			"finish", 
			"play", 
			"undo" if s.buffer.size() > 0 else "delete"
		)
		Tutorial.update_states(
			1,
			1 if s.hovered == -1 else 0,
			1 if s.buffer.empty() else 0,
			1 if s.buffer.size() > 0 else 0,
			1 if s.buffer.empty() else 0,
			1 if s.hovered > -1 or s.buffer.size() > 0 else 0
		)
	else:
		if s.hovered == -1:
			Tutorial.update_states(2,1,1,0,0,0)
		else:
			if s.grabbed == -1:
				Tutorial.update_states(1,2,1,0,0,0)
			else:
				Tutorial.update_states(1,1,2,0,0,0)

func clear(s : Mapel.State):
	level.clear()
	editor.clear()
	s.level = -1
	s.shapes = []

func select_level(s : Mapel.State, index : int):
	if s.level != index:
		editor.visible = true
		if index < 0 or index >= s .levels.size():
			clear(s)
			run(Cmd.show_rotate, true)
		else:
			run(Cmd.show_rotate, false)
			s.shapes = s.levels[index]
			s.level = index
			s.cursor = $CursorStart.position

			background.set_material(level_materials[index])

			level.clear()
			init(s)

			if not s.editing:
				level.init(s)
				run(Cmd.start)

			editor.show_filled(s.editing)

func set_static(s : Mapel.State, st : bool):
	if s.editing:
		s.buffer = []
		s.static = st
		run(Cmd.set_editing, s.editing)

func set_editing(s : Mapel.State, e : bool, force : bool = false):
	if config.editing_enabled or force:
		s.buffer = []
		level.clear()
		s.editing = e
		editor.show_filled(s.editing)
		editor.visible = true

		if s.editing:
			init(s)
			update_tutorial(s)
			run(Cmd.show_rotate, false)
		else:
			Tutorial.update_labels("move", "grab", "rotate", "finish", "edit" if config.editing_enabled else "shake", "undo")
			s.buffer = []
			level.init(s)
			run(Cmd.select_level, s.level)
			run(Cmd.start)


func init(s : Mapel.State):
	# shape_material.set_shader_param("color_a", Color("#ee7766"))
	# shape_material.set_shader_param("color_b", Color("#00cc99"))
	# shape_material.set_shader_param("color_hover", Color("#00cc99"))
	level.clear()
	level.add_statics(s)
	s.shapes = s.levels[s.level]
	editor.init(s.shapes, level_materials[s.level])

func start(s : Mapel.State):
	run(Cmd.show_rotate, false)
	s.cursor = $CursorStart.position
	s.editing = false
	editor.visible = true

	level.init(s)
	s.buffer = []
	editor.show_filled(false)
	Tutorial.update_labels("move", "grab", "rotate", "finish", "edit" if config.editing_enabled else "shake", "undo")

	# start_timer.start()
	run(Cmd.explode)

func turn(s : Mapel.State, direction : int):
	if not s.editing:
		if s.grabbed > -1:
			s.grab_offset = level.turn(s.grabbed, s.cursor, direction)

func undo(s : Mapel.State):
	if s.editing:
		if not s.buffer.empty():
			s.buffer.pop_back()
		elif s.hovered > -1:
			if s.hovered_is_static:
				s.statics.remove(s.hovered)
			else:
				s.shapes.remove(s.hovered)
			s.hovered = -1
			run(Cmd.init)
			save(s)

func grab(s : Mapel.State):
	if not s.editing:
		level.try_grab(s)

func drop(s : Mapel.State):
	if not s.editing:
		level.try_drop(s)

func on_idle():
	if not state.editing:
		run(Cmd.start)

func move(s : Mapel.State, dir : Vector2):
	var nt = s.cursor + config.move_speed * dir 
	s.cursor = Util.clamp_point_in_rect(nt, Vector2.ZERO, Util.window_size())
	if s.editing:
		s.hovered = editor.hovered_poly(s)
		if not s.hovered_is_static and s.shapes.size() > 0:
			var h : float = float(s.hovered) / float(s.shapes.size())
			level_materials[s.level].set_shader_param("hovered", h)
			level_materials[s.level].set_shader_param("grabbed", 0.0 if s.grabbed == -1 else 1.0)
	# else:
	# 	update_hovered(s)


func place(s : Mapel.State):
	if s.editing and s.level > -1:
		if editor.cursor_placement_valid(s):
			# print(s.cursor)
			s.buffer.push_back(s.cursor)

func shake(s : Mapel.State):
	randomize()
	if not s.editing:
		level.shake()

func explode(s : Mapel.State):
	randomize()
	if not s.editing:
		level.explode()


func finish(s : Mapel.State):
	if s.buffer.size() > 2:
		var tris = Geometry.triangulate_polygon(PoolVector2Array(s.buffer))
		if tris.size() > 0:
			var shape = Mapel.Poly.new(s.buffer)
			if shape.valid():
				if s.static:
					s.statics.push_back(shape)
				else:	
					s.shapes.push_back(shape)

				run(Cmd.init)
				save(s)
			else:
				print("invalid shape")
			s.buffer = []

		else:
			print("couldn't create shape")


func run(command : int, arg = null):
	if command != Cmd.move:
		print("[cmd] ", Cmd.keys()[command], " ", arg if arg != null else "")
	# print(Cmd.keys()[command])
	command_stack.push_back([command, arg])

func show_rotate(s : Mapel.State, b : bool):
	level.show_rotate(b)
	if b:
		editor.visible = false
		background.set_material(grid_material)
	else:
		if s.level > -1:
			editor.visible = true
			background.set_material(level_materials[s.level])
	Tutorial.show_rotate(b, not b)

func on_end():
	# run(Cmd.start)
	run(Cmd.show_rotate, true)

func run_serial_command(command):
	print("[server] ", Cmd.keys()[command[0]], " ", command[1])
	command_stack.push_back(command)

func _ready():
	# if Engine.editor_hint:
	# 	print("in editor")

	for k in Cmd.keys():
		commands.push_back(funcref(self, k))

	state = Mapel.State.new()
	state.load(Util.load_json("world.json"))
	config = Mapel.Config.new(Util.load_json("config.json"))

	server = MapelServer.new(config.socket_server_port)
	server.connect("message", self, "run_serial_command")
	add_child(server)

	level.ready(state, config, level_materials)
	level.connect("ended",self, "on_end")
	level.connect("idled", self, "on_idle")

	editor.show_filled(state.editing)

	background.rect_size = Util.window_size()
	background.set_material(level_materials[0])


	if config.launch_node:
		node_pid = OS.execute(config.node_path, ["arduino_desk/serial_socket.js", config.socket_server_port], false)

	OS.set_window_fullscreen(config.fullscreen)
		

	# if OS.get_name() == "HTML5":
	# 	OS.window_fullscreen = true
	# print(config)
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	run(Cmd.set_editing, config.editing_enabled)
	run(Cmd.select_level, 0)
	
func _notification(n):
	if n == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		if node_pid:
			OS.kill(node_pid)
		get_tree().quit()

func _input(e):
	level.start_idling()

	if e is InputEventMouseMotion:
		run(Cmd.move, e.relative)
	elif e is InputEventMouseButton:
	
		if e.pressed:
			print("[mouse] ", e.button_index)
			match e.button_index:
				BUTTON_LEFT:
					if state.editing:
						run(Cmd.place)
					else:
						run(Cmd.grab)
				BUTTON_RIGHT:
					run(Cmd.undo)
				BUTTON_WHEEL_UP:
					if state.editing:
						# run(Cmd.set_static, not state.static)
						pass
					else:
						run(Cmd.turn, -1)
				BUTTON_WHEEL_DOWN:
					if state.editing:
						# run(Cmd.set_static, not state.static)
						pass
					else:
						run(Cmd.turn, 1)
				BUTTON_MIDDLE:
					run(Cmd.set_static, not state.static)
					print_ip()
			input_stack = (input_stack + str(e.button_index)).substr(max(0,input_stack.length() - edit_cheat.length() + 1))
			print(input_stack)
			if input_stack == edit_cheat:
				set_editing(state, not state.editing, true)
		else:
			match e.button_index:
				BUTTON_LEFT:
					if not state.editing:
						run(Cmd.drop)
				BUTTON_MIDDLE:
					$IP.visible = false

	elif e is InputEventKey:
		if e.pressed:
			if not e.echo:
				print("[key] ", e.as_text())
				match e.as_text():
					"Space":
						if state.editing:
							run(Cmd.place)
						else:
							run(Cmd.grab)
					"Escape":
						if config.editing_enabled:
							run(Cmd.set_editing, not state.editing)
						else:
							run(Cmd.shake)
					"Enter" : 
						if state.editing:
							run(Cmd.place)
							run(Cmd.finish)
						else:
							run(Cmd.start)
					"S" : run(Cmd.set_static, not state.static)
					"E" : run(Cmd.set_editing, not state.editing)
					"T" : run(Cmd.shake)
					"R" : run(Cmd.explode)
					_ :
						var i = e.as_text().to_int()
						if i != null and e.as_text().length() == 1:
							i -= 1
							if i < 5:
								run(Cmd.select_level, i)
		else:
			match e.as_text():
				"Space":
					if not state.editing:
						run(Cmd.drop)


func draw_buffer():
	var width = 2.0
	var color = Color(1,1,1,1) if editor.cursor_placement_valid(state) else Color(1,0,0,1)

	if state.buffer.size() == 1:
		draw_line(state.buffer[0], state.cursor, color, width)
	else:
		for i in state.buffer.size():
			var p = state.buffer[i]
			# print(p)
			if i == state.buffer.size() - 1:
				draw_line(p, state.cursor, color, width)
				draw_line(state.cursor, state.buffer[(i + 1) % state.buffer.size()], color, width)
			else:
				draw_line(p, state.buffer[(i + 1) % state.buffer.size()], color, width)

func _draw():
	var s = 10
	var width = 3.0
	var t = 0.5 + 0.5 * sin((1.0 - (cursor_timer.time_left / cursor_timer.wait_time)) * TAU * 1.0)
	var cursor_color = Color(t,t,t,1.0)
	if state.editing:
		if state.static:
			draw_arc(state.cursor, s * 3, 0, TAU, 5, cursor_color, 2)
		else:
			draw_arc(state.cursor, s * 3, 0, TAU, 32, cursor_color, 2)
	draw_line(state.cursor - Vector2(s, s), state.cursor + Vector2(s, s), cursor_color, width)
	draw_line(state.cursor - Vector2(s, -s), state.cursor + Vector2(s, -s), cursor_color, width)

	draw_buffer()


func eval_command(s : Mapel.State, c):
	if c[1] != null:
		commands[c[0]].call_func(s, c[1])
	else:
		commands[c[0]].call_func(s)

func eval_commands(s : Mapel.State):
	var moves = []
	for i in range(command_stack.size() - 1,-1,-1):
		if command_stack[i][0] == Cmd.move:
			moves.push_back(command_stack.pop_at(i))

	var all = command_stack.size() + moves.size()
	
	for c in command_stack:
		eval_command(s, c)
	command_stack = []

	for c in moves:
		eval_command(s, c)
	
	if all > 0 or s.editing:
		update_tutorial(s)

func _process(_delta):
		# get_tree().quit()
	server.update()
	update()
	var c = state.cursor / Util.window_size()
	level_materials[state.level].set_shader_param("cursor", c)
	grid_material.set_shader_param("cursor", c)

func _physics_process(_delta):
	eval_commands(state)
	if not state.editing and state.level > -1:
		if level.update_hovered(state):
			update_tutorial(state)

		level.try_drag(state)
		if level.try_ending(state):
			editor.visible = false
			Tutorial.show_rotate(false, false)

"""
win turn scene
bottom shape collision
show turn help
"""
