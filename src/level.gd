class_name Level
extends Node2D

signal ended()
signal idled()

onready var shapes = $Shapes
onready var border = $Border
onready var statics = $Statics
onready var end_timer = $EndTimer
onready var start_timer = $StartTimer
onready var idle_timer = $IdleTimer
onready var end_player : AudioStreamPlayer = $EndPlayer
onready var grab_player : AudioStreamPlayer = $GrabPlayer
onready var drop_player : AudioStreamPlayer = $DropPlayer
onready var hover_player : AudioStreamPlayer = $HoverPlayer
onready var level_player : AudioStreamPlayer = $LevelPlayer

var config = null

var won : bool = false

# var shape_material

var level_materials = []

var static_material : ShaderMaterial = preload("res://mats/static.tres")

var placeholder_texture

var ignored_colliders = []

func start_idling():
	idle_timer.start()

func on_idle():
	print("[level] idling out")
	emit_signal("idled")

func on_end():
	print("[level] ended")
	Util.delete_children(shapes)
	emit_signal("ended")

func show_rotate(b : bool):
	for s in statics.get_children():
		s.get_child(0).visible = b
		s.get_child(1).visible = not b
	
	static_material.set_shader_param("pulse", 1.0 if b else 0.0)

func add_statics(s : Mapel.State):
	Util.delete_children(statics)
	for shape in s.statics:
		statics.add_child(shape.generate_static(static_material))
	
	level_player.play()




func ready(state : Mapel.State, c : Mapel.Config, materials : Array):
	config = c
	level_materials = materials
	shapes.visible = not state.editing

	add_borders(Util.window_size())
	add_statics(state)

	# start_timer.connect("timeout", self, "on_idle")

	idle_timer.connect("timeout", self, "on_idle")
	idle_timer.wait_time = config.idle_duration
	idle_timer.start()

	end_timer.connect("timeout", self, "on_end")
	end_timer.wait_time = config.end_duration


func _ready():
	placeholder_texture = Util.placeholder_texture()
	pass

func add_rect(b : Node, p : Vector2, w : Vector2):
	var body = RigidBody2D.new()
	body.set_mode(RigidBody2D.MODE_STATIC)
	body.can_sleep = false
	var shape = RectangleShape2D.new()
	var collider = CollisionShape2D.new()
	shape.extents = w
	collider.set_shape(shape)
	body.add_child(collider)
	body.physics_material_override = PhysicsMaterial.new()
	body.physics_material_override.bounce = 0.5
	body.physics_material_override.friction = 0.2

	body.global_position = p
	b.add_child(body)

func add_borders(s : Vector2):
	add_rect(border, Vector2(s.x * 0.5, - s.y * 0.5), Vector2(s.x, s.y * 0.5))
	add_rect(border, Vector2(s.x * 0.5,   s.y + s.y * 0.5), Vector2(s.x, s.y * 0.5))
	add_rect(border, Vector2(- s.x * 0.5, s.y * 0.5), Vector2(s.x * 0.5, s.y))
	add_rect(border, Vector2(s.x + s.x * 0.5, s.y * 0.5), Vector2(s.x * 0.5, s.y))

func clear():
	Util.delete_children(shapes)
	won = false
	end_timer.stop()
	start_timer.stop()


func init(s : Mapel.State):
	clear()
	static_material.set_shader_param("pulse", 0.0)
	end_timer.stop()
	end_player.stop()
	won = false
	var i = 0
	for shape in s.shapes:
		shapes.add_child(PolyBody.new(shape, float(i) / float(s.shapes.size()), level_materials[s.level]))
		i += 1

	add_statics(s)

	ignored_colliders = []
	for body in border.get_children():
		ignored_colliders.push_back(body)
	for body in statics.get_children():
		ignored_colliders.push_back(body)

func turn(index : int, point : Vector2, direction : int):
	var b = shapes.get_child(index)

	var d = direction * config.rotate_speed
	var a = b.rotation + d

	var cs = cos(d)
	var ss = sin(d)

	var bp = b.global_position - point
	var p = point + Vector2(bp.x * cs - bp.y * ss, bp.x * ss + bp.y * cs)

	b.rotation = a
	b.global_position = p
	return point - p

func snap_body(b : RigidBody2D, s : Mapel.Poly):
	b.set_mode(RigidBody2D.MODE_KINEMATIC)
	b.get_child(1).disabled = true
	b.global_position = s.center
	b.rotation = 0
	# b.z_index = -1
	b.play_snap()
	b.get_child(0).color.r = 1.0
	b.state = 2
	b.z_index = 0
	# b.get_node("CollisionPolygon2D").disabled = true

func body_solved(b : RigidBody2D, s : Mapel.Poly):
	var distance_solved = s.center.distance_to(b.global_position) < config.snap_distance
	var a = abs(b.rotation) / TAU
	a = a - floor(a)
	var angle_solved = a < config.snap_angle or a > 1.0 - config.snap_angle
	return distance_solved and angle_solved

func solve_shapes(s : Mapel.State):
	var i = 0
	var solved = 0
	for shape in s.shapes:
		var b = shapes.get_child(i)
		match b.state:
			0 :
				if not body_solved(b, shape):
					b.state = 1
			1 : 
				if body_solved(b, shape):
					print("[level] solved ", i)
					if s.grabbed == i:
						try_drop(s)
					snap_body(b, shape)
			2:
				solved += 1
		i += 1
	return solved == s.shapes.size() and s.shapes.size() > 0

func hovered_body(point : Vector2):
	var space_state = get_world_2d().direct_space_state
	var hovered = space_state.intersect_point(point, 1, ignored_colliders)
	if hovered:
		return hovered[0].collider.get_index()
	else:
		return -1

func random_force(b, f = 1000.0):
	b.apply_central_impulse(Vector2(randf() * 2.0 - 1.0, randf() * 2.0 - 1.0).normalized() * f)
	b.apply_torque_impulse(randf() * 10.0)

func shake(_f = 1000.0):
	for b in shapes.get_children():
		if b.state != 2:
			random_force(b)
			# if b.mode != RigidBody2D.MODE_RIGID:
			# 	b.set_mode(RigidBody2D.MODE_RIGID)


func explode(_f = 1000.0):
	for b in shapes.get_children():
		random_force(b)

func try_grab(s : Mapel.State):
	if s.grabbed == -1 and s.hovered > -1:
		s.grabbed = s.hovered
		s.grab_offset = s.cursor - shapes.get_child(s.grabbed).global_position
		level_materials[s.level].set_shader_param("grabbed", 1.0)

		var b = shapes.get_child(s.grabbed)
		b.set_mode(RigidBody2D.MODE_KINEMATIC)
		b.z_index = 2
		grab_player.play()

func try_drop(s : Mapel.State):
	if s.grabbed > -1:
		var b = shapes.get_child(s.grabbed)
		b.set_mode(RigidBody2D.MODE_RIGID)
		b.z_index = 1
		s.grabbed = -1
		level_materials[s.level].set_shader_param("grabbed", 0.0)
		drop_player.play()
	
func try_drag(s : Mapel.State):
	if s.grabbed > -1:
		# level.cursor.global_position = state.cursor - state.grab_offset
		shapes.get_child(s.grabbed).global_position = s.cursor - s.grab_offset

func try_ending(s : Mapel.State) -> bool:
	if start_timer.is_stopped():
		if not won:
			won = solve_shapes(s)
			if won:
				end_timer.start()
				end_player.play()

		if s.level > -1:
			if won and not end_timer.is_stopped():
				var t : float = 1.0 - (end_timer.time_left / end_timer.wait_time)
				level_materials[s.level].set_shader_param("win_time", t)
				for s in shapes.get_children():
					s.scale += Vector2(0.01, 0.01)
				return true
	return false

func update_hovered(s : Mapel.State) -> bool:
	var hovered = hovered_body(s.cursor)
	if hovered != s.hovered:
		s.hovered = hovered
		if s.shapes.size() > 0:
			hover_player.play()
			var h : float = float(s.hovered) / float(s.shapes.size())
			level_materials[s.level].set_shader_param("hovered", h)
			level_materials[s.level].set_shader_param("grabbed", 0.0 if s.grabbed == -1 else 1.0)
		return true
	return false

