extends RigidBody2D
class_name PolyBody

var drag_sound = preload("res://sound/drag.ogg")
var snap_sound = preload("res://sound/unsnap.ogg")

var audio = null

enum State{
	INIT,
	UNSOLVED,
	SOLVED
}

var state = State.INIT

func _ready():
	audio = AudioStreamPlayer2D.new()
	audio.stream = drag_sound
	audio.set_volume_db(-80)
	add_child(audio)
	audio.play()

func play_snap():
	audio.stream = snap_sound
	audio.set_pitch_scale(0.6)
	audio.play()
	audio.set_volume_db(0)

func _physics_process(delta):
	# pass
	if state == State.UNSOLVED:
		var v = clamp(linear_velocity.length() / 1000.0, 0.001, 1.0)
		# print(v)
		
		# 	audio
		audio.set_pitch_scale(v)

		var vol = 10 * log( v ) / log( 2 )
		# print(vol)
		audio.set_volume_db(clamp(vol + 15, -80, 0))

func _init(shape, float_index : float, mat : ShaderMaterial):
	# body.set_mode(RigidBody2D.MODE_KINEMATIC)
	set_mode(RigidBody2D.MODE_RIGID)
	global_position = shape.center
	
	var poly = Polygon2D.new()
	poly.set_polygon(shape.polygon)
	poly.set_uv(shape.uv)
	poly.set_material(mat)
	poly.set_texture(Util.placeholder_texture())
	poly.set_color(Color(0.0, 1.0, float_index, 1.0))
	var collider = CollisionPolygon2D.new()
	collider.set_polygon(shape.polygon)
	
	add_child(poly)
	add_child(collider)
	# body.continuous_cd