class_name Editor
extends Node2D

static func hovered_in_array(cursor : Vector2, shapes) -> int:
	var i = 0
	for shape in shapes:
		if Geometry.is_point_in_polygon(cursor - shape.center, shape.polygon):
			return i
		i += 1
	return -1

static func hovered_poly(s : Mapel.State) -> int:
	var index := hovered_in_array(s.cursor, s.shapes)
	if index > -1:
		s.hovered_is_static = false
		return index
	else:
		index = hovered_in_array(s.cursor, s.statics)
		if index > -1:
			s.hovered_is_static = true
		return index

static func cursor_placement_valid(s : Mapel.State):
	var nt = PoolVector2Array(s.buffer)
	nt.append(s.cursor)
	if nt.size() == 1:
		for shape in s.shapes:
			if Geometry.is_point_in_polygon(s.cursor, shape.global_polygon()):
				return false
	elif nt.size() >= 2:
		for shape in s.shapes:
			if Geometry.intersect_polyline_with_polygon_2d([nt[nt.size() - 2], nt[nt.size() - 1]], shape.global_polygon()):
				return false
	return true

func _ready():
	pass

func show_filled(b : bool):
	for c in get_children():
		c.get_child(0).visible = b

func clear():
	Util.delete_children(self)


func init(shapes, mat : ShaderMaterial):
	clear()
	var i = 0
	for shape in shapes:
		var outline = Line2D.new()
		outline.set_width(2.0)
		var op = PoolVector2Array(shape.polygon)
		var mid_point = lerp(shape.polygon[0], shape.polygon[shape.polygon.size() - 1], 0.5)
		op.insert(0, mid_point)
		op.insert(op.size(), mid_point)
		outline.set_points(op)
		outline.set_default_color(Color(0.2, 0.7, 0.5, 1.0))

		var poly = Polygon2D.new()
		poly.set_texture(Util.placeholder_texture())
		poly.set_polygon(shape.polygon)
		poly.set_color(Color(0.0, 1.0, float(i) / float(shapes.size())))
		# poly.set_color(Color(randf(), 0.0, float(i) / float(s.shapes.size())))
		# print(shape.uv)
		poly.set_uv(shape.uv)
		# poly.set_uv(shape.uv)
		poly.set_material(mat)
		poly.show_behind_parent = true
		outline.add_child(poly)
		add_child(outline)
		outline.global_position = shape.center
		i += 1
