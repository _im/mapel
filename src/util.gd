class_name Util
extends Object

static func clamp_point_in_rect(p, r, s):
	return Vector2(clamp(p.x, r.x, s.x), clamp(p.y, r.y, s.y))

static func window_size():
	return Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))

static func delete_children(node):
	for n in node.get_children():
		node.remove_child(n)
		n.queue_free()

static func save_json(p : String, data):
	var file = File.new()
	file.open(p, File.WRITE)
	file.store_line(to_json(data))
	file.close()

static func load_json(p : String):
	var file = File.new()
	# var user_path = "user://" + p
	var user_path = "user://" + p
	var default_path = "res://" + p
	if not file.file_exists(user_path):
		print(p, " doesn't exist, creating...")
		var default_file = File.new()
		default_file.open(default_path, File.READ)
		var defaults = parse_json(default_file.get_as_text())
		print(defaults)
		save_json(user_path, defaults)
		return defaults

	file.open(user_path, File.READ)
	var d = parse_json(file.get_as_text())
	# print(file.get_as_text())
	# file.close()
	return d
	
static func placeholder_texture() -> Texture:
	var image = Image.new()
	image.create(2,2, false, false)

	var texture = ImageTexture.new()
	texture.create_from_image(image)
	return texture
	
static func full_quad(m : ShaderMaterial, z : int) -> Polygon2D:
	var s = window_size()
	var poly := Polygon2D.new()
	poly.set_polygon(PoolVector2Array([
		Vector2(0,0),
		Vector2(s.x,0),
		Vector2(s.x,s.y),
		Vector2(0,s.y),
	]))
	poly.set_uv(PoolVector2Array([
		Vector2(0,0),
		Vector2(1.0,0.0),
		Vector2(1.0,1.0),
		Vector2(0,1.0),                                 
	]))

	poly.set_texture(placeholder_texture())
	poly.set_color(Color(0.0, 0.0, 100.0, 1.0))
	poly.set_material(m)
	poly.z_index = z
	return poly

