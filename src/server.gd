extends Node
class_name MapelServer

var _server = WebSocketServer.new()
var socket_port : int = 8000
signal message(arr)

func _init(port : int):
  socket_port = port

func _ready():
  # Connect base signals to get notified of new client connections,
  # disconnections, and disconnect requests.
  _server.connect("client_connected", self, "_connected")
  _server.connect("client_disconnected", self, "_disconnected")
  _server.connect("client_close_request", self, "_close_request")
  # This signal is emitted when not using the Multiplayer API every time a
  # full packet is received.
  # Alternatively, you could check get_peer(PEER_ID).get_available_packets()
  # in a loop for each connected peer.
  _server.connect("data_received", self, "_on_data")
  # Start listening on the given port.
  var err = _server.listen(socket_port)
  if err != OK:
	  print("Unable to start server")
	  set_process(false)

func _connected(id, proto):
  # This is called when a new peer connects, "id" will be the assigned peer id,
  # "proto" will be the selected WebSocket sub-protocol (which is optional)
  print("Client %d connected with protocol: %s" % [id, proto])
  emit_signal("message", [0, -1])

func _close_request(id, code, reason):
  # This is called when a client notifies that it wishes to close the connection,
  # providing a reason string and close code.
  print("Client %d disconnecting with code: %d, reason: %s" % [id, code, reason])

func _disconnected(id, was_clean = false):
  # This is called when a client disconnects, "id" will be the one of the
  # disconnecting client, "was_clean" will tell you if the disconnection
  # was correctly notified by the remote peer before closing the socket.
  print("Client %d disconnected, clean: %s" % [id, str(was_clean)])

func _on_data(id):
  # Print the received packet, you MUST always use get_peer(id).get_packet to receive data,
  # and not get_packet directly when not using the MultiplayerAPI.
  # print(id)
  var packet = _server.get_peer(id).get_packet()
  var string = packet.get_string_from_utf8()
  var message = parse_json(string)
  # print(message)
  emit_signal("message", message)
  # print("[ socket ] (id : %d) -> %s" % [id, message.command])
  # echo
  # _server.get_peer(id).put_packet(packet)

func update():
  _server.poll()
