extends Control

onready var platform : Control = $Platform
onready var platform_frames : Control = $Platform/Frames
onready var platform_label : Label = $Platform/Label
onready var mouse : Control = $Mouse
onready var labels : Control = $Mouse/Labels
onready var buttons : Control = $Mouse/Buttons
onready var scale_timer : Timer = $ScaleTimer

export var lines := false

enum State {
	HIDE,
	SHOW,
	ACCENT
}

var label_states := {
	move = State.ACCENT,
	grab = State.SHOW,
	turn = State.SHOW,
	start = State.HIDE,
	shake = State.HIDE,
	undo = State.HIDE,
}

func set_label(i:int, t:String):
	labels.get_child(i).get_child(0).text = t

func update_labels(mv:String, gr:String, tr:String, st:String, sh:String, un:String):	
	set_label(0, mv)
	set_label(1, gr)
	set_label(2, tr)
	set_label(3, st)
	set_label(4, sh)
	set_label(5, un)

func update_states(mv:int, gr:int, tr:int, st:int, sh:int, un:int):	
	label_states.move = mv
	label_states.grab = gr
	label_states.turn = tr
	label_states.start = st
	label_states.shake = sh
	label_states.undo = un
	for b in buttons.get_children():
		b.visible = label_states[b.name] == State.ACCENT


func _process(_delta):
	update()

func wave() -> float:
	return sin(time() * TAU * 1.0) * 0.5 + 0.5

func show_label(l : Control, accent : bool):
	l.visible = true
	var label = l.get_child(0)
	if lines:
		draw_line(l.rect_position, l.rect_position + label.rect_position + label.rect_pivot_offset, Color.white)

	if accent:
		var s = 0.2
		var w = wave()
		label.rect_scale = Vector2.ONE * (1.0 + s * w)
		buttons.get_node(l.name).modulate = Color(1,1,1, w)
	else:
		label.rect_scale = Vector2.ONE
func time():
	return 1.0 - (scale_timer.time_left / scale_timer.wait_time)
func frame() -> int:
	return int(floor(time() * (platform_frames.get_child_count())))

func _draw():
	if platform.visible:
		var f = frame()
		for i in platform_frames.get_child_count():
			platform_frames.get_child(i).visible = f == i
		platform_label.rect_scale = Vector2.ONE * (1.0 + wave() * 0.2)



	if mouse.visible:
		for l in labels.get_children():
			match label_states[l.name]:
				State.HIDE:
					l.visible = false
				State.SHOW:
					show_label(l, false)
				State.ACCENT:
					show_label(l, true)

func show_rotate(p:bool, m:bool):
	platform.visible = p
	mouse.visible = m
