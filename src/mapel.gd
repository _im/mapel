class_name Mapel
extends Object

class Config extends Reference:
	var socket_server_port := 9080
	var move_speed := 1.0
	var idle_duration := 150
	var end_duration := 15
	var rotate_speed := 0.1
	var snap_distance := 20
	var snap_angle := 0.01
	var editing_enabled := false
	var launch_node := false
	var node_path := "node"
	var fullscreen := false
	func _init(j):
		print("[config]")
		for k in j.keys():
			self[k] = j[k]
			print("  ", k, " : ", j[k])

class State extends Reference:
	var cursor := Vector2.ZERO
	var grabbing := false
	var hovering := false
	var static := false
	var hovered_is_static := false
	var hovered := -1
	var grabbed := -1
	var grab_offset := Vector2.ZERO
	var editing := false
	var buffer = []
	var level := -2
	var levels = [[],[],[],[],[],[],[],[],[],[]]
	var shapes = []
	var statics = []
	func save() -> Dictionary:
		var world = {
			levels = [],
			statics = []
		}
		levels[level] = shapes
		
		for l in levels:
			var ls = []
			for shape in l:
				ls.push_back(shape.save())
			world.levels.push_back(ls)

		for shape in statics:
			world.statics.push_back(shape.save())

		return world

	func load_level(d):
		var a = []
		for s in d:
			var poly = Poly.from(s)
			if poly.valid():
				a.push_back(poly)
		return a

	func load(data):
		levels = []
		for l in data.levels:
			levels.push_back(load_level(l))

		while levels.size() < 5:
			levels.push_back([])
		
		statics = []
		for shape in data.statics:
			statics.push_back(Poly.from(shape))

class Poly extends Reference:
	var uv 
	var polygon
	var center : Vector2
	func _init(buffer):
		center = Poly.get_polygon_center(buffer)
		polygon = Poly.offset_polygon_points(center, buffer)
		uv = Poly.points_to_uv(buffer)
	func valid() -> bool:
		# position (-3, 2), size (1, 1)
		var rect := Rect2(Vector2.ZERO, Vector2.ZERO)
		for p in polygon:
			rect = rect.expand(p)
		return rect.get_area() > 9

	static func points_to_uv(points, offset = Vector2(0,0)):
		var uvs = PoolVector2Array()
		var s = Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))
		for p in points:
			# uvs.append(p + offset)
			uvs.append((p + offset) / s)
		return uvs

	static func from(d):
		var ps = PoolVector2Array()
		for p in d.polygon:
			ps.append(Vector2(p[0], p[1]))
		var p = Poly.new([])
		p.center = Vector2(d.center[0], d.center[1])
		p.polygon = ps
		p.uv = Poly.points_to_uv(ps, p.center)
		return p

	static func vec2array(v):
		return [v.x, v.y]

	static func poly2array(ps):
		var a = []
		for p in ps:
			a.push_back(vec2array(p))
		return a
	
	func save():
		return {"center" : vec2array(center), "polygon" : poly2array(polygon)}

	func global_polygon():
		var r = []
		for p in polygon:
			r.append(p + center)
		return r

	func generate_static(mat : ShaderMaterial) -> StaticBody2D:
		var body := StaticBody2D.new()
		var shape := CollisionPolygon2D.new()
		var poly := Polygon2D.new()

		var outline = Line2D.new()
		outline.set_width(2.0)
		var op = PoolVector2Array(polygon)
		var mid_point = lerp(polygon[0], polygon[polygon.size() - 1], 0.5)
		op.insert(0, mid_point)
		op.insert(op.size(), mid_point)
		outline.set_points(op)
		outline.set_default_color(Color.white)
		body.add_child(outline)
		outline.visible = false
		outline.material = mat

		shape.set_polygon(polygon)
		poly.set_polygon(polygon)
		poly.uv = Poly.points_to_uv(polygon)
		body.add_child(poly)
		body.add_child(shape)
		body.position = center
		poly.material = mat

		# body.z_index = -1
		return body

	static func offset_polygon_points(center_pos: Vector2, poly):
		var adjusted_points = []
		for point in poly:
			adjusted_points.append(point - center_pos)
		return adjusted_points

	static func get_polygon_center(poly):
		var center_weight = poly.size()
		var center_pos = Vector2(0, 0)
		for point in poly:
			center_pos.x += point.x / center_weight
			center_pos.y += point.y / center_weight
		return center_pos

